package main;

/**
 * Created by Настеныш on 30.11.2016.
 */
public class Config {
    public static final String DB_CONNECTION = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
//    public static final String DB_INIT_SCRIPT_FILE = "create-tables-with-data.sql";
    public static final String DB_INIT_SCRIPT_FILE = "clothing-store.sql";
    public static final int WEB_SERVER_PORT = 8080;

    public static final int MAIN_PAGE_INDEX = 0;
    public static final int ABOUT_PAGE_INDEX = 1;
    public static final int CATALOG_PAGE_INDEX = 2;
}
