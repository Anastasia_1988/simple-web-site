package main;

import controller.AboutCompanyServlet;
import controller.CompanyCatalogServlet;
import controller.MainPageServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.h2.tools.RunScript;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.DriverManager;

import static main.Config.*;

/**
 * Created by Настеныш on 29.11.2016.
 */
public class Main {
    public static void main(String[] args) throws Exception {
        InputStream inputStream = Main.class.getClassLoader().getResourceAsStream(DB_INIT_SCRIPT_FILE);
        RunScript.execute(DriverManager.getConnection(DB_CONNECTION), new InputStreamReader(inputStream));

        MainPageServlet mainPageServlet = new MainPageServlet();
        AboutCompanyServlet aboutCompanyServlet = new AboutCompanyServlet();
        CompanyCatalogServlet companyCatalogServlet = new CompanyCatalogServlet();

        //обработчик контекста сервлета
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);

        context.addServlet(new ServletHolder(mainPageServlet), "/");
        context.addServlet(new ServletHolder(aboutCompanyServlet), "/about.html");
        context.addServlet(new ServletHolder(companyCatalogServlet), "/catalog.html");

        Server server = new Server(WEB_SERVER_PORT);
        server.setHandler(context);

        server.start();              // метод для старта сервера;
        server.join();               // метод для присоединения сервера к пулу потоков;
    }
}
