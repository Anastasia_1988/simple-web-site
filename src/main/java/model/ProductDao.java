package model;

import main.Config;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Настеныш on 30.11.2016.
 */
public class ProductDao {
    public static List<Product> getAll() {
        List<Product> products = new ArrayList<>();
        String queryString = "SELECT * FROM PRODUCT ORDER BY NAME ASC";
        try (Connection db = DriverManager.getConnection(Config.DB_CONNECTION)) {
            try (Statement sqlQuery = db.createStatement()) {
                ResultSet resultSet = sqlQuery.executeQuery(queryString);
                while (resultSet.next()) {
                    Product product = new Product();
                    product.setName(resultSet.getString(2));
                    product.setDescription(resultSet.getString(3));
                    product.setPrice(resultSet.getDouble(4));
                    product.setImage(resultSet.getString(5));
                    product.setQuantity(resultSet.getInt(6));
                    products.add(product);
                }
                resultSet.close();
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return products;
    }
}
