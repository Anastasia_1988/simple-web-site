package model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Настеныш on 30.11.2016.
 */
public class Page {
    private String title;
    private String description;
    private Date created;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public Page() {
    }

    public Page(String title, String description, Date created) {
        this.title = title;
        this.description = description;
        this.created = created;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Map<String, Object> getParams() {
        Map<String, Object> map = new HashMap<>();
        map.put("title", title);
        map.put("description", description);
        map.put("created", dateFormat.format(created));
        return map;
    }
}
