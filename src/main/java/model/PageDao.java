package model;

import main.Config;

import java.sql.*;

/**
 * Created by Настеныш on 30.11.2016.
 */
public class PageDao {
    public static Page getPageById(Integer id) {
        Page page = null;
        String queryString = "SELECT * FROM PAGE WHERE ID = " + id + "";
        try (Connection db = DriverManager.getConnection(Config.DB_CONNECTION)) {
            try (Statement sqlQuery = db.createStatement()) {
                ResultSet resultSet = sqlQuery.executeQuery(queryString);
                page = new Page();
                while (resultSet.next()) {
                    page.setTitle(resultSet.getString(2));
                    page.setDescription(resultSet.getString(3));
                    page.setCreated(resultSet.getDate(4));
                }
                resultSet.close();

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return page;
    }
}
