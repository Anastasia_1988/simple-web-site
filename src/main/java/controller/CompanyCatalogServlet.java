package controller;

import model.PageDao;
import model.ProductDao;
import view.PageGenerator;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

import static main.Config.CATALOG_PAGE_INDEX;

/**
 * Created by Настеныш on 29.11.2016.
 */
public class CompanyCatalogServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map<String, Object> map = PageDao.getPageById(CATALOG_PAGE_INDEX).getParams();
        map.put("products", ProductDao.getAll());
        response.setContentType("text/html; charset=utf-8");
        response.getWriter().println(PageGenerator.instance().getPage("catalog.html", map));
    }
}
