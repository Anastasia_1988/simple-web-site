package controller;

import model.PageDao;
import view.PageGenerator;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static main.Config.MAIN_PAGE_INDEX;

/**
 * Created by Настеныш on 29.11.2016.
 */
public class MainPageServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html; charset=utf-8");
        response.getWriter().println(PageGenerator.instance().getPage("index.html",
                PageDao.getPageById(MAIN_PAGE_INDEX).getParams()));
    }
}

