CREATE TABLE PAGE (ID INT PRIMARY KEY, TITLE VARCHAR(512), DESCRIPTION VARCHAR(10000), CREATED DATE);
CREATE TABLE PRODUCT (ID INT PRIMARY KEY, NAME VARCHAR(512), DESCRIPTION VARCHAR(1000), PRICE DOUBLE, IMAGE VARCHAR(10000), QUANTITY INT);

INSERT INTO PAGE VALUES(0, 'Главная страница', 'Самые красивые платья по привлекательной цене', '2016-11-29');
INSERT INTO PAGE VALUES(1, 'О компании', 'Мы привозим только самые лучшие платья из Европы', '2016-11-30');
INSERT INTO PAGE VALUES(2, 'Каталог компании', 'Платья женские', '2016-11-30');

INSERT INTO PRODUCT VALUES(0, 'Платье-миди', 'Короткое приталенное платье миди с кружевом. Цвет кремовый', 4384.61, 'http://images.asos-media.com/products/korotkoe-pritalennoe-plate-midi-s-kruzhevom-asos-salon/6029425-1-cream?$S$&wid=100&fit=constrain',8);
INSERT INTO PRODUCT VALUES(1, 'Платье-макси', 'Платье макси с длинными рукавами TFNC Tall Wedding', 4230.76, 'http://images.asos-media.com/products/plate-maksi-s-dlinnymi-rukavami-tfnc-tall-wedding/6955860-1-navy?$S$&wid=100&fit=constrain', 2);
INSERT INTO PRODUCT VALUES(2, 'Платье винтажное', 'Винтажное платье из кружевы длины миди с цветочным принтом с рукавами 3/4', 5032.21, 'http://d3sej37t1mx5mv.cloudfront.net/image/85_116/50/2b/502bc927db476b0c9057b3e295ba1562.jpg', 1);
