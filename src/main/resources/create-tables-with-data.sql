CREATE TABLE PAGE (ID INT PRIMARY KEY, TITLE VARCHAR(512), DESCRIPTION VARCHAR(10000), CREATED DATE);
CREATE TABLE PRODUCT (ID INT PRIMARY KEY, NAME VARCHAR(512), DESCRIPTION VARCHAR(1000), PRICE DOUBLE, IMAGE VARCHAR(10000), QUANTITY INT);

INSERT INTO PAGE VALUES(0, 'Главная страница', 'Это описание из базы данных', '2016-11-29');
INSERT INTO PAGE VALUES(1, 'О компании', 'Краткая история компании', '2016-11-30');
INSERT INTO PAGE VALUES(2, 'Каталог мебели', 'Товары', '2016-11-30');

INSERT INTO PRODUCT VALUES(0, 'Стол', 'Офисный', 5500.00, 'http://www.dialux-help.ru/images/catalog/ae5eb633048e1ce2a414257d29671b32.jpg',8);
INSERT INTO PRODUCT VALUES(1, 'Шкаф', 'Для бумаг ', 9900.00, 'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcSsXejaqv2BAKCYPrhYIHp5y7DXxlBmTrHuNc2wpzP6ZxguGyNBOw', 2);
INSERT INTO PRODUCT VALUES(2, 'Кресло', 'Кресло руководителя', 47000.00, 'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcTPqTuaRxXzg2h9pBO1OqOV7jFkbEb9aeCEGeAxeJVC_ejfZBd8ZQ', 1);
